const { Build, By, Key, util, Builder, WebElement, WebDriver, Actions } = require("selenium-webdriver");
const { FileDetector } = require('selenium-webdriver/remote');

(async function test() {
    const capabilities = {
        'browserName':'firefox',
        'browser_version':'latest',
        'name': 'test ayd',
        'build': 'aydrive',
        'vide':'true'
    } 
    let drive;
    drive = await new Builder().usingServer('http://34.125.236.190:4444').withCapabilities(capabilities).build();
    //drive = await new Builder().forBrowser("firefox").build();

    try {
        /*
            await registro_de_usuario(drive);
            await registro_de_usuario(drive);
            await autenticacion_incorrecta(drive);
            await editar(drive);
        */
        await nombre_edad(drive);
        await foro(drive);
        await console.log("Pruebas funcionales con éxito")
    } catch (e) {
        console.log(e)
        console.log("Algo pasa aqui, unlucky!!!")
        throw e
    } finally {
        await drive.quit()
    }
})();

async function nombre_edad(drive) {
    try {
        await drive.get("http://34.125.236.190:80");
        await drive.findElement(By.name("nombre")).sendKeys("christian");
        await drive.findElement(By.name("edad")).sendKeys("21");
        await drive.findElement(By.name("b1")).click()
    } catch (error) {
        console.log(error)
        console.log("Algo pasa aqui2, unlucky!!!")
        throw error
    }
}

async function foro(drive) {
    try {
        await drive.get("http://34.125.236.190:80");
        await drive.findElement(By.name("correo")).sendKeys("christian.rizo1@gmail.com");
        await drive.findElement(By.name("mensajeforo")).sendKeys("Mi carnet es: 201800580");
        await drive.findElement(By.name("b2")).click()
    } catch (error) {
        console.log(error)
        console.log("Algo pasa aqui3, unlucky!!!")
        throw error
    }
}

